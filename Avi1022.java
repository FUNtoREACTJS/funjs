package com.socgen.avi.util;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.mutable.MutableInt;

import com.socgen.avi.context.ApplicationContext;
import com.socgen.avi.dossier.TDoF0Sfd;
import com.socgen.avi.dossier.TDoF4Sfd;
import com.socgen.avi.dossier.TDoPSfd;
import com.socgen.avi.dossier.TEcrMini;
import com.socgen.avi.dossier.TRep314;
import com.socgen.avi.ihm.bean.TRecEdicpt;
import com.socgen.avi.pojo.Avi1022rensSup;
import com.socgen.avi.pojo.TEcr;
import com.socgen.avi.pojo.TQue314;
import com.socgen.avi.pojo.Txte;
import com.socgen.sng.cent.cv.Dcv;
import com.socgen.sng.cent.cv.DcvCat;
import com.socgen.sng.cent.cv.DcvEntry;
import com.socgen.sng.cent.gd.GD;
import com.socgen.sng.cent.gd.GdContext;
import com.socgen.sng.cent.gd.TDosStructure;
import com.socgen.sng.cent.gd.TGdAction;
import com.socgen.sng.cent.gp.GP;
import com.socgen.sng.cent.gp.TGpAction;
import com.socgen.sng.cent.gp.TGpSpecif;
import com.socgen.sng.cent.gp.TPEtdInterdit;
import com.socgen.sng.cent.tr.TR;
import com.socgen.sng.cent.tr.TTrOk;
import com.socgen.sng.core.cdn.TCCg;
import com.socgen.sng.core.cdn.TDes;
import com.socgen.sng.core.cdn.TFilsOrder;
import com.socgen.sng.core.mutable.MutableString;
import com.socgen.sng.util.DateUtil;
import com.socgen.sng.util.StringUtil;
import com.socgen.sng.util.bf.Bf;
import com.socgen.sng.util.context.SessionContext;
import com.socgen.ue.Ue;
import com.socgen.ue.pojo.TAppInEnt;
import com.socgen.ue.pojo.TAppInSort;
import com.socgen.ue.pojo.TUePcb;

public final class Avi1022 {

	public static final String C_MNEMO = "AVI";

	public static final String VERS_STRG = "1022";

	public static final int VERSNBR = 1000;

	public static final String BIB_ECR = "AVI1020A.XQTF";

	public static final int MAX_ECR = 99;

	public static final int CAT_DO = 10200;

	public static final int FILS1 = 0;

	public static final int FILS2 = 1;

	public static final int CAT_DO_SFD = 10466;

	public static final int F0_SFD = 0;

	public static final int F4_SFD = 4;

	public static final int CAT_DP_PAG = 15315;

	public static final int CAT_DP_SAI = 15810;

	public static void init() {

		int version;
		TDosStructure structure = new TDosStructure();
		version = 0;

		GD.gdInit(SessionContext.getInstance().getContext().getPageOmf().getOmf(), 13,
				SessionContext.getInstance().getContext().getDefUser().getUser(),
				SessionContext.getInstance().getContext().getDatejour(), version); 
		GP.gpInit(SessionContext.getInstance().getContext().getPageOmf().getOmf(), 13,
				SessionContext.getInstance().getContext().getDatejour(), Avi1022.VERSNBR); 
		GD.gdFixcat(Avi1022.CAT_DO, structure);
		ApplicationContext.getInstance().getAvi1022Context().setAbandon(GD.getGdresult() != 0);
		ApplicationContext.getInstance().getAvi1022Context().setMem1("");
		ApplicationContext.getInstance().getAvi1022Context().setMem2("");
		ApplicationContext.getInstance().getAvi1022Context().setMem3("");
		ApplicationContext.getInstance().getAvi1022Context().setMulti(false);
	}

	public static void finit() {
		GP.gpFinish();
		GD.gdFinish();
	}

	public void int314(final MutableString titulaire_Mutable, final MutableBoolean toutOk_Mutable, final TCCg cCg) {

		String titulaire = titulaire_Mutable.stringValue();
		boolean toutOk = toutOk_Mutable.booleanValue();
		int codQuestion;
		TDes destinataire = new TDes();
		TTrOk retTrInterro = TTrOk.OK;
		TRep314 rep314 = new TRep314();
		TQue314 int314 = new TQue314();
		String libelle = StringUtils.EMPTY;
		/* (*int_client *) */
		Dcv tabint = initTabint();
		toutOk = false;
		destinataire.setFrm(0);
		destinataire.getDes1().setCtiOmfSl(0);

		codQuestion = 314;
		int314.getEntete().setVerItr(1);
		int314.getEntete().setOmfUser(SessionContext.getInstance().getContext().getDefUser().getUser());
		int314.getEntete().setDItr(SessionContext.getInstance().getContext().getDatejour());
		int314.setAgenceCompt(cCg.getAgence());
		int314.setAgence(cCg.getAgence());
		int314.setGr(cCg.getGrc());
		int314.setCp(cCg.getCpar());
		retTrInterro = TR.trInterro(destinataire, codQuestion, tabint, int314, rep314);
		if (retTrInterro == TTrOk.OK) {
			if (rep314.getCodeRetour() == 0) {
				titulaire = rep314.getIntitule();
				toutOk = true;
			} else {
				libelle = TR.getRet(rep314.getCodeRetour());
				Bf.smesg(libelle);
				/* (* interro *) */
			}
		}
		titulaire_Mutable.setValue(titulaire);
		toutOk_Mutable.setValue(toutOk);
	}

	/**
	 * (*v1020*) (* APPEL A APPLI LOCAL IN*) (*v1020*)
	 */
	public static void testAppliIn(final MutableInt codeRetour_Mutable, final MutableBoolean bool_Mutable) {

		int codeRetour = codeRetour_Mutable.intValue();
		boolean bool = bool_Mutable.booleanValue();
		TAppInEnt entAppli = new TAppInEnt();
		TAppInSort donApp = new TAppInSort();
		int monHandle;
		TUePcb pcbUe = new TUePcb();
		Date dateDuJour;
		/* test_appli_in */
		entAppli.setMonAppli("AVI");
		entAppli.setMonEtiq("BASCULE OPT");
		dateDuJour = SessionContext.getInstance().getContext().getDatejour();
		entAppli.setDateInter(dateDuJour);
		monHandle = SessionContext.getInstance().putData(entAppli);
		pcbUe.setHandleIn(monHandle);
		Ue.ueAppliIn(pcbUe);

		monHandle = pcbUe.getHandleOut();
		donApp = (TAppInSort) SessionContext.getInstance().getData(monHandle);
		SessionContext.getInstance().delData(monHandle);
		codeRetour = donApp.getCodeRetour();
		bool = donApp.isIndEuro();
		/* fin test_appli_in */
		codeRetour_Mutable.setValue(codeRetour);
		bool_Mutable.setValue(bool);
	}

	private static Dcv initTabint() {
		Dcv tabint = new Dcv(2);
		DcvCat cat001 = tabint.getDescripteurs()[0];
		cat001.setCatint(314);

		DcvEntry entry001001 = cat001.getEntries()[0];
		entry001001.getHeader().setNbrFormat(8);
		entry001001.getHeader().setOccurence(false);
		entry001001.getHeader().setLastHeader(true);
		byte[] formats001001 = new byte[8];
		entry001001.setFormats(formats001001);
		formats001001[0] = 1;
		formats001001[1] = 1;
		formats001001[2] = 6;
		formats001001[3] = 1;
		formats001001[4] = 3;
		formats001001[5] = 3;
		formats001001[6] = 3;
		formats001001[7] = 4;
		DcvCat cat002 = tabint.getDescripteurs()[1];
		cat002.setCatint(5314);

		DcvEntry entry002001 = cat002.getEntries()[0];
		entry002001.getHeader().setNbrFormat(7);
		entry002001.getHeader().setOccurence(false);
		entry002001.getHeader().setLastHeader(true);
		byte[] formats002001 = new byte[7];
		entry002001.setFormats(formats002001);
		formats002001[0] = 1;
		formats002001[1] = 1;
		formats002001[2] = 6;
		formats002001[3] = 1;
		formats002001[4] = 0;
		formats002001[5] = 9;
		formats002001[6] = 17;
		/* (* init_tabint *) */

		return tabint;
	}

	public static void creerDp(final TPEtdInterdit ref, final TGpSpecif gpSpeci, final String re1, final String re2,
			final String re3, final Txte txte, final long refer) {

		gpSpeci.getRefPere().setCategorie(Avi1022.CAT_DP_SAI);
		gpSpeci.getRefPere().setNumero(refer);
		gpSpeci.getRefPere().setOrder(0);
		GP.gpAction(TGpAction.GP_RES_PERE, Avi1022.CAT_DP_SAI, gpSpeci, ref);
		ApplicationContext.getInstance().getAvi1022Context().setAbandon(GP.getGpresult() != 0);
		ApplicationContext.getInstance().getAvi1022Context().getF().setDoF0Sfd(GP.getWindow(TDoF0Sfd.class));
		if (!ApplicationContext.getInstance().getAvi1022Context().isAbandon()) {
			ApplicationContext.getInstance().getAvi1022Context().getF().getDpSai().setRef1(re1);
			ApplicationContext.getInstance().getAvi1022Context().getF().getDpSai().setRef2(re2);
			ApplicationContext.getInstance().getAvi1022Context().getF().getDpSai().setRef3(re3);
			ApplicationContext.getInstance().getAvi1022Context().getF().getDpSai().setTxt(txte);
			GP.gpAction(TGpAction.GP_FAST_CREAT_PERE, Avi1022.CAT_DP_SAI, gpSpeci, ref);
			ApplicationContext.getInstance().getAvi1022Context().setAbandon(GP.getGpresult() != 0);
			if (!ApplicationContext.getInstance().getAvi1022Context().isAbandon()) {
				Bf.smesg("Sauvegarde ok");
			}
		}
	}

	private static String strMois(final int m, final String mois) {

		String result = mois;
		switch (m) {
		case 1:
			result = "Janvier";
			break;
		case 2:
			result = "Février";
			break;
		case 3:
			result = "Mars";
			break;
		case 4:
			result = "Avril";
			break;
		case 5:
			result = "Mai";
			break;
		case 6:
			result = "Juin";
			break;
		case 7:
			result = "Juillet";
			break;
		case 8:
			result = "Août";
			break;
		case 9:
			result = "Septembre";
			break;
		case 10:
			result = "Octobre";
			break;
		case 11:
			result = "Novembre";
			break;
		case 12:
			result = "Décembre";
			break;
		default:
			break;
		}
		return result;
	}

	public static void compt(final boolean cptCli, final int nbEx, final String cpt, final int k,
			final TRecEdicpt edicpt) {

		String s = StringUtils.EMPTY;
		String annee = StringUtils.EMPTY;
		if (cptCli) {
			edicpt.setT1("┌───────┬────────┬─────┬────┐");
			edicpt.setT2("│       │        │     │    │");
			edicpt.setT3("└───────┴────────┴─────┴────┘");
			/*
			 * (*v1020*) (*v1020*)
			 */
			ApplicationContext.getInstance().getAvi1022Context().setXserie(StringUtil.copy(cpt, 13, 3));
			/* (*v1020*) */
			edicpt.setT2(StringUtil.moveLeft(edicpt.getT2(), 26, 2, cpt, cpt.length() - 1));
			edicpt.setT2(StringUtil.moveLeft(edicpt.getT2(), 20, 3, cpt, cpt.length() - 5));
			edicpt.setT2(StringUtil.moveLeft(edicpt.getT2(), 11, 6, cpt, cpt.length() - 12));
			edicpt.setT2(StringUtil.moveLeft(edicpt.getT2(), 4, 4, cpt, 1));

			/* (*v1020*) */
			if (ApplicationContext.getInstance().getDgeunitContext().isdBascul()) {
				/* (*v1020*) */
				/* (*v1020*) */
				if (!"003".equals(ApplicationContext.getInstance().getAvi1022Context().getXserie())
						&& !"004".equals(ApplicationContext.getInstance().getAvi1022Context().getXserie())
						&& !"012".equals(ApplicationContext.getInstance().getAvi1022Context().getXserie())
						&& !"013".equals(ApplicationContext.getInstance().getAvi1022Context().getXserie())
						&& !"015".equals(ApplicationContext.getInstance().getAvi1022Context().getXserie())
						&& !"043".equals(ApplicationContext.getInstance().getAvi1022Context().getXserie())
						&& !"044".equals(ApplicationContext.getInstance().getAvi1022Context().getXserie())) {
					/* (*v1020*) */
					/* (*v1020*) */
					/* (*v1020*) */
					/* (*v1020*) */
					edicpt.setFacturation("Cet avis ne tient pas lieu de facture");
				}
			}
		} else {
			/* (*v1020*) */
			edicpt.setT1("┌───────┬───────┬────────┐");
			edicpt.setT2("│       │       │        │");
			edicpt.setT3("└───────┴───────┴────────┘");
			edicpt.setT2(StringUtil.moveLeft(edicpt.getT2(), 19, 6, cpt, cpt.length() - 5));
			edicpt.setT2(StringUtil.moveLeft(edicpt.getT2(), 11, 5, cpt, cpt.length() - 11));
			edicpt.setT2(StringUtil.moveLeft(edicpt.getT2(), 5, 3, cpt, cpt.length() - 14));
		}
		ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[k].setExDem(nbEx);
		edicpt.setParisle("le ");
		s = String.valueOf(
				DateUtil.getDay(ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[k].getDatcpta()));
		edicpt.setParisle(edicpt.getParisle() + s + " ");
		s = strMois(DateUtil.getMonth(ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[k].getDatcpta()),
				s);
		/*
		 * (*v1020*) (********* CORRECTION **********) (*v1020*) (*DATE ERRONNEE
		 * SUR LES EDITIONS *) (*v1020*) (* Affichage pour l'annÚe de 20106 au
		 * lieu de 2006 *)
		 */
		/*
		 * (*v1020*) (*if sav_ecr[k].datcpta.y > 27 then*) (*v1020*)
		 */
		if (DateUtil.getYear(ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[k].getDatcpta()) > 127
				&& DateUtil.getYear(
						ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[k].getDatcpta()) <= 100) {
			/* (*v1020*) */
			edicpt.setParisle(edicpt.getParisle() + s + " 19");
		} else {
			edicpt.setParisle(edicpt.getParisle() + s + " 20");
		}
		/*
		 * (*v1020*) (*str ( sav_ecr[k].datcpta.y , s ) ;*) (*v1020*)
		 */
		annee = String.valueOf(
				DateUtil.getYear(ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[k].getDatcpta()));
		/* (*v1020*) */
		s = StringUtil.copy(annee, 3, 2);
		edicpt.setParisle(edicpt.getParisle() + s);
		if (ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[k].getMontant() < 0) {
			edicpt.setAvisde("AVIS DE DEBIT");
		} else {
			edicpt.setAvisde("AVIS DE CREDIT");
		}
	}

	public static void recupEcr() {
		ApplicationContext.getInstance().getAvi1022Context().setSavEcr(new TEcr[0]);
		ApplicationContext.getInstance().getAvi1022Context().setRensSup(new Avi1022rensSup[0]);
		if (GD.getEntetePere().getEtat().isExecute()) {
			ApplicationContext.getInstance().getAvi1022Context().setDoEx(true);
		} else {
			ApplicationContext.getInstance().getAvi1022Context().setDoEx(false);
		}
		GdContext.get().setEndOfFils(false);
	}

	public static void recupSfd() {

		TFilsOrder[] filsSuiv;
		Date datCpt;
		int nbDec;
		boolean ok = false;
		int occ;
		int occurs;
		int j;
		int i;
		for (TEcr savEcr : ApplicationContext.getInstance().getAvi1022Context().getSavEcr()) {
			savEcr.clear();
		}
		for (Avi1022rensSup rensSup : ApplicationContext.getInstance().getAvi1022Context().getRensSup()) {
			rensSup.clear();
		}
		if (GD.getEntetePere().getEtat().isExecute()) {
			ApplicationContext.getInstance().getAvi1022Context().setDoEx(true);
		} else {
			ApplicationContext.getInstance().getAvi1022Context().setDoEx(false);
		}
		GdContext.get().setEndOfFils(false);
		j = 0;

		TDoPSfd doSfd = GD.getWindow(TDoPSfd.class);
		filsSuiv = doSfd.getConstante().getOrdFMini();
		ApplicationContext.getInstance().getAvi1022Context()
				.setMulti(doSfd.getConstante().getTypeLot()[0]);
		datCpt = doSfd.getConstante().getDateComptable();
		nbDec = doSfd.getConstante().getEquilibre().getNbDecimale();
		occurs = doSfd.getOccurs();
		for (occ = 0; occ <= occurs - 1; occ++) {
			j = recEcr(doSfd.getEcrMini()[occ], datCpt, nbDec, ok, j);
		}
		i = 0;
		while (i < 3 && !ApplicationContext.getInstance().getAvi1022Context().isAbandon()) {
			if (filsSuiv[i].getNumero() > 0) {
				appelFils(filsSuiv, i);
				if (!ApplicationContext.getInstance().getAvi1022Context().isAbandon()) {
					ApplicationContext.getInstance().getAvi1022Context().getF().setDoF0Sfd(GD.getWindow(TDoF0Sfd.class));
					occurs = ApplicationContext.getInstance().getAvi1022Context().getF().getDoF0Sfd().getOccurs();
					for (occ = 0; occ <= occurs - 1; occ++) {
						j = recEcr(ApplicationContext.getInstance().getAvi1022Context().getF().getDoF0Sfd().getEcrMiniFils()[occ], datCpt, nbDec, ok, j);
					}
				}
			}
			i = i + 1;
		}
		if (ApplicationContext.getInstance().getAvi1022Context().isMulti()) {
			i = appelF4(j, i);
		}
	}

	// MIGRE_NON_INTEGRE_DEB
	/*
	 * Le code en commentaire est issu de Psystem suite à la migration vers ONYX. Il n’est pas intégré pour la version actuelle de l’application. 
	 * Il doit rester en l’état car il est susceptible d’être utilisé pour une évolution fonctionnelle liée à la réactivation d’une fonctionnalité 
	 * désactivée dans une version antérieure PSYSTEM 
	 */	
//	private static boolean fils(final MutableInt j_Mutable) {

//		int j = j_Mutable.intValue();
//		boolean result = true;
//		if (result) {
//			ApplicationContext.getInstance().getAvi1022Context().getRensSup()[j].setDevIso("FRF");
//			ApplicationContext.getInstance().getAvi1022Context().getRensSup()[j].setNbDec(2);
//			if (ApplicationContext.getInstance().getAvi1022Context().getEntete().getFils().getOrder()
//					.getCategorie() == 0) {
//
//				TEcr tEcr = ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[j];
//				tEcr.setCptCli(true);
//				// tEcr.setMontant(ApplicationContext.getInstance().getAvi1022Context().getEntete().getFils().getMont());
//				// Cdn.cdCdnPack(ApplicationContext.getInstance().getAvi1022Context().getF().getTFils1().getResp(),
//				// tEcr.getCpt());
//				// tEcr.setDat(ApplicationContext.getInstance().getTFils1().getDatVal());
//				// tEcr.setDatcpta(ApplicationContext.getInstance().getTFils1().getDatCpt());
//				// tEcr.setCodOp(ApplicationContext.getInstance().getTFils1().getNatEcr());
//				// (ApplicationContext.getInstance().getTFils1().getBool()[0]) {
//				j++;
//			} else {
//				if (ApplicationContext.getInstance().getAvi1022Context().getEntete().getFils().getOrder()
//						.getCategorie() == 1) {
//
//					TEcr tEcr = ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[j];
//					tEcr.setCptCli(false);
//					// tEcr.setMontant(ApplicationContext.getInstance().getTFils2().getMont());
//					// Cdn.cdCptaPack(ApplicationContext.getInstance().getTFils2().getResp(),
//					// tEcr.getCpt());
//					// tEcr.setDatcpta(ApplicationContext.getInstance().getTFils2().getDatCpt());
//					j++;
//				}
//			}
//		}
//		result = GD.getGdresult() == 0 && !GD.isEndOfFils();
//		j_Mutable.setValue(j);
//		return result;
//	}

//	private static boolean accesBase(final MutableBoolean firstP_Mutable) {
//		boolean firstP = firstP_Mutable.booleanValue();
//		boolean result;
//		if (firstP) {
//			firstP = false;
//			GD.gdAction(TGdAction.FIRST_FILS, GD.I_REFFILS,
//					ApplicationContext.getInstance().getAvi1022Context().getEntete());
//		} else {
//			GD.gdAction(TGdAction.NEXT_FILS, GD.I_REFFILS,
//					ApplicationContext.getInstance().getAvi1022Context().getEntete());
//		}
//		result = GD.getGdresult() == 0 && !GD.isEndOfFils();
//		firstP_Mutable.setValue(firstP);
//		return result;
//	}
	// MIGRE_NON_INTEGRE_DEB

	private static void appelFils(final TFilsOrder[] filsSuiv, final int i) {
		GD.getGdTarget().setFilsRef(filsSuiv[i]);
		GD.gdAction(TGdAction.GET_FILS, GD.I_REFFILS, ApplicationContext.getInstance().getAvi1022Context().getEntete());
		if (GD.getGdresult() != 0) {
			Bf.smesg("Fils inaccessible ");
			ApplicationContext.getInstance().getAvi1022Context().setAbandon(true);
		}
	}

	private static int recEcr(final TEcrMini ecrit, final Date datCpt, final int nbDec, final boolean ok, final int j) {

		int result = j;

		TEcr tEcr = ApplicationContext.getInstance().getAvi1022Context().getSavEcr()[j];
		if (ecrit.getDivers()[0]) {
			tEcr.setCptCli(true);
			tEcr.setCpt(ecrit.getAgenceResp() * 1000000000 + ecrit.getClassOuCg() * 1000 + ecrit.getSerie());
			tEcr.setCpt(tEcr.getCpt() * 100 + ecrit.getsCompte());
		} else {
			tEcr.setCptCli(false);
			tEcr.setCpt(
					ecrit.getAgenceResp() * 100000000000L + ecrit.getClassOuCg() * 1000000 + ecrit.getnChqOuCpar());
		}
		tEcr.setMontant(ecrit.getMontant());
		tEcr.setDat(ecrit.getDateValeur());
		tEcr.setDatcpta(datCpt);
		tEcr.setCodOp(ecrit.getCodeLib());
		if (ecrit.getPresence()[1]) {
			tEcr.setExo(true);
		} else {
			tEcr.setExo(false);
		}
		ApplicationContext.getInstance().getAvi1022Context().getRensSup()[result].setDevIso(ecrit.getMonnaie());
		if (!ApplicationContext.getInstance().getAvi1022Context().isMulti()) {
			ApplicationContext.getInstance().getAvi1022Context().getRensSup()[result].setNbDec(nbDec);
		}
		result = result + 1;
		return result;
	}

	private static int appelF4(final int j, final int i) {

		int result = i;
		int jj;

		TFilsOrder tFilsOrder = GD.getGdTarget().getFilsRef();
		tFilsOrder.setNumero(1);
		tFilsOrder.setCategorie(4);
		GD.gdAction(TGdAction.GET_FILS, GD.I_REFFILS, ApplicationContext.getInstance().getAvi1022Context().getEntete());
		if (ApplicationContext.getInstance().getAvi1022Context().getEntete().getFils().getOrder().getCategorie() == 4) {
			for (jj = 0; jj <= j - 1; jj++) {
				result = 0;
				/* $R- */
				TDoF4Sfd doF4Sfd = GD.getWindow(TDoF4Sfd.class);
				while (result <= doF4Sfd.getAjuParDev().getOccurs()) {
					if (doF4Sfd.getAjuParDev().getEquilibre()[result].getCtrEquilibre()
							.getMonnaie().equals(ApplicationContext.getInstance().getAvi1022Context().getRensSup()[jj]
									.getDevIso())) {
						ApplicationContext.getInstance().getAvi1022Context().getRensSup()[jj]
								.setNbDec(doF4Sfd.getAjuParDev().getEquilibre()[result].getNbDecimale());
						result = doF4Sfd.getAjuParDev().getOccurs() + 1;
					} else {
						result = result + 1;
						/* $R+ */
					}
				}
			}
		}
		return result;
	}
}
